import { Router } from 'preact-router'
import { hashLinkScroll } from 'utils/router'
import Layout from 'components/layout/base'
import Home from 'containers/home'
import DES from 'containers/des'
import AES from 'containers/aes'
import RSA from 'containers/rsa'

const App = () => {
  return (
    <Layout>
      <Router onChange={hashLinkScroll}>
        <Home path='/' />
        <DES path='/des' />
        <AES path='/aes' />
        <RSA path='/rsa' />
      </Router>
    </Layout>
  )
}

export default App
