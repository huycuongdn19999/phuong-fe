import { Text, Icon, PseudoBox, Flex } from '@chakra-ui/core'
import { Link } from 'preact-router'
import PropTypes from 'prop-types'

export default function AppItem ({
  icon,
  name,
  color,
  isActive,
  href,
  activePaths,
  ...props
}) {
  return (
    <PseudoBox
      as={Link}
      d='flex'
      direction='row'
      alignItems='center'
      px='md'
      py='sm'
      borderRadius='4px'
      bg={isActive && 'brand.background'}
      color={isActive && 'brand.primary'}
      href={href}
      fontSize='bodyLarge'
      _hover={{
        bg: 'brand.background',
        color: 'brand.primary',
        cursor: 'pointer'
      }}
      _first={{ mt: 0 }}
      {...props}
    >
      <Flex
        w='32px'
        height='32px'
        borderRadius='8px'
        backgroundColor={color}
        color='white'
        alignItems='center'
        justifyContent='center'
        mr='xs'
      >
        <Icon size='20px' name={icon} />
      </Flex>
      <Text fontSize='H200' lineHeight='H200' fontWeight='medium'>
        {name}
      </Text>
    </PseudoBox>
  )
}

AppItem.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  name: PropTypes.string,
  isActive: PropTypes.bool,
  href: PropTypes.string,
  activePaths: PropTypes.array
}
