import { memo } from 'preact/compat'
import { Box, Flex } from '@chakra-ui/core'
import MenuList from './MenuList'

const LayoutBase = ({ children, menuActiveId = 1, title = 'Incidents' }) => {
  const menus = [
    {
      id: 4,
      icon: 'dashboard',
      title: 'Homepage',
      href: '/'
    },
    {
      id: 3,
      icon: 'station',
      title: 'DES',
      href: '/des'
    },
    {
      id: 2,
      title: 'aes-128-ecb',
      href: '/aes',
      icon: 'incidentComponent'
    },
    {
      id: 5,
      title: 'RSA',
      href: '/rsa',
      icon: 'incidentComponent'
    }
  ]
  return (
    <Flex pt='60px' backgroundColor='white'>
      <Box mb='xxl' flex={1}>
        {children}
      </Box>
      <Box
        backgroundColor='white'
        borderTop='1px'
        borderColor='gray.200'
        position='fixed'
        left={0}
        right={0}
        bottom={0}
        height='64px'
      >
        <MenuList menus={menus} />
      </Box>
    </Flex>
  )
}

export default memo(LayoutBase)
