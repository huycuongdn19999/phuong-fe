import { Stack, Heading, Icon, Link } from '@chakra-ui/core'

export default function HeadingBack ({ title, isBack, children }) {
  return (
    <Stack
      position='fixed'
      top='0px'
      left='0px'
      right='0px'
      alignItems='center'
      spacing='sm'
      isInline
      // backgroundColor='#fff'
      borderBottom='1px'
      borderBottomColor='gray.200'
      height='60px'
      zIndex='99'
      px='sm'
      background='linear-gradient(135deg, rgb(29, 137, 206) 0%, rgb(86, 210, 243) 100%)'
    >
      {isBack && (
        <Link p='sm' onClick={() => window.history.back()}>
          <Icon name='chevronLeft' color='#fff' size='16px' />
        </Link>
      )}

      <Heading mb={0} color='#fff' fontSize='H500' lineHeight='H500'>
        {title || children}
      </Heading>
    </Stack>
  )
}
