import { memo } from 'preact/compat'
import {
  Input as VInput,
  FormErrorMessage,
  FormControl,
  FormLabel
} from '@chakra-ui/core'

const Input = ({ field, form, ...props }) => {
  return (
    <FormControl
      {...props}
      isInvalid={form.errors[field.name] && form.touched[field.name]}
    >
      <FormLabel pb='xxs' fontSize='H200' htmlFor={field.name}>
        {props.label}
      </FormLabel>
      <VInput {...field} {...props} mb='sm' />
      <FormErrorMessage>{form.errors[field.name]}</FormErrorMessage>
    </FormControl>
  )
}

export default memo(Input)
