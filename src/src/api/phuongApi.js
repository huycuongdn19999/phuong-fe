
import { getPhuongApi } from '../config'
import { getFetch, postFetch } from 'utils/fetch'

const baseUrl = getPhuongApi()
export const genKey = () => {
    const apiUrl = baseUrl + '/genKey'
    return getFetch(apiUrl)
}
export const encryptText = (text, key) => {
    const apiUrl = baseUrl + '/encryptText'
    return postFetch(apiUrl, { text, key })
}
export const decode = (text, key) => {
    const apiUrl = baseUrl + '/decode'
    return postFetch(apiUrl, { text, key })
}