module.exports = {
  apps: [
    {
      name: 'iLotusLand Incident App',
      script: 'npm run serve -- -p 4200',
      // Options reference: http`s://pm2.io/doc/en/runtime/reference/ecosystem-file/
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '1G',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'development',
        PORT: 4200,
        MONGODB_URI:
          'mongodb+srv://ilotusland_incidents:MWlNeRauAlXMlgHV@cluster0.4yjou.mongodb.net/ilotusland_incidents?retryWrites=true&w=majority'
      }
    }
  ]
}
